
export interface IAnswer {
  id: string
  userProfile: string
  name: string
  likeCount: number
  dislikeCount: number
  date: Date
  answer: string
}
export interface IQuestion {
  id: string;
  userProfile: string;
  title: string;
  date: Date;
  description: string;
  answers: IAnswer[]
}
