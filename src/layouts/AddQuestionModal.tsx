import axios from "axios";
import { FC } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { addQuestionSwitch, refetchQuestions, selectAddQuestionSwitch } from '../store/slices/questionSlice'
interface IProps {

}
const QuestionModal: FC<IProps> = () => {
    const dispatch = useDispatch()
    const questionSlice = useSelector(selectAddQuestionSwitch)
    const submit = (event: any) => {
        event.preventDefault()
        let form = new FormData(event.currentTarget)
        const subject = form.get("subject")
        const description = form.get("description") ?? ""
        if (subject) {
            let data = {
                title: subject,
                description: description,
                id: String(Date.now()),
                date: new Date(),
                userProfile: "../../assets/images/profile.png",
                answers: []
            }
            axios.post(`http://localhost:5000/questions`, data).then(res => {
                if (res.status === 200) {
                    dispatch(refetchQuestions() as any)
                    close()
                }
            })
        }
    }

    const close = () => {

        dispatch(addQuestionSwitch(false))
    }
    return (
        <>
            {questionSlice && (
                <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                    <h3 className="text-3xl font-semibold">
                                        ایجاد سوال جدید
                                    </h3>
                                    <button
                                        className=""
                                        onClick={close}
                                    >
                                        <span className="">
                                            X
                                        </span>
                                    </button>
                                </div>
                                <form onSubmit={submit} className="relative p-6 flex-auto" id="add-question-form">
                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2">
                                            موضوع
                                        </label>
                                        <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="subject" type="text" />
                                    </div>
                                    <div className="mb-4">
                                        <label className="block text-gray-700 text-sm font-bold mb-2">
                                            متن سوال
                                        </label>
                                        <textarea rows={5} cols={80} className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="description" />
                                    </div>
                                </form>
                                <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                                    <button
                                        className="text-green-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={close}
                                    >
                                        انصراف
                                    </button>
                                    <button
                                        className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="submit"
                                        form="add-question-form"
                                    >
                                        ایجاد سوال
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                </>
            )}
        </>
    );
}

export default QuestionModal