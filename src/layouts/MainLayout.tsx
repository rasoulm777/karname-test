import { FC, ReactNode } from 'react'
import profile from '../assets/images/navbarImg.png'
import AddQuestionModal from './AddQuestionModal'
import { useDispatch, useSelector } from 'react-redux';
import { addQuestionSwitch, selectNavbarTitle } from '../store/slices/questionSlice'
import Icons from '../assets/icons/Icons';
interface IProps {
  children: ReactNode
}
const MainLayout: FC<IProps> = ({ children }) => {
  const dispatch = useDispatch()
  const navbarTitle = useSelector(selectNavbarTitle)
  const showAddModal = () => {
    dispatch(addQuestionSwitch(true))
  }
  return (
    <>
      <div className='grid grid-cols-2 gap-4 shadow-lg py-4 px-4'>
        <div className='flex items-center'>
          <h3 className='font-bold text-xl pr-2' >{navbarTitle}</h3>
        </div>
        <div className='grid grid-cols-1 place-items-end'>
          <div className='flex items-center'>
            <button className="bg-green-500 ml-11 hover:bg-green-400 max-w-fit text-white font-bold py-2 px-4 rounded flex items-center whitespace-nowrap" onClick={showAddModal}>
              <Icons icon='plus' />
              <span className='pb-1'>سوال جدید</span>
            </button>
            <img src={profile} alt='profile' className='w-10 h-10 rounded-full' />
            <p className='pr-2 font-bold'>الناز شاکر دوست</p>
            <button className="text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
              <Icons icon='arrow-bottom' />
            </button>
          </div>
        </div>
      </div>
      <div className='grid grid-cols-1 min-h-screen p-10 bg-slate-100'>
        {children}
      </div>
      <AddQuestionModal />
    </>
  )
}

export default MainLayout