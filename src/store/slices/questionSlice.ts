import createGenericSlice from "../genericSlice";
import { GenericState } from "../../models/common/common";
import { PayloadAction } from "@reduxjs/toolkit";
import { IQuestion } from "./../../models/questions/questions";
import axios from "axios";
import { Dispatch } from "react";

interface IQuestionSlice {
  questionList: IQuestion[];
  navbarTitle: string;
  addQuestionModalSwitch: boolean;
}

const defaultValues: IQuestionSlice = {
  questionList: [],
  addQuestionModalSwitch: false,
  navbarTitle: "1لیست سوالات",
};

export const slice = createGenericSlice({
  name: "question",
  initialState: {
    status: "loading",
    data: defaultValues,
  } as GenericState<IQuestionSlice>,
  reducers: {
    addQuestionSwitch: (state, action: PayloadAction<boolean>) => {
      state.status = "finished";
      state.data = {
        questionList: state.data?.questionList ?? [],
        addQuestionModalSwitch: action.payload,
        navbarTitle: state.data?.navbarTitle ?? "لیست سوالات",
      };
    },
    setQuestionList: (state, action: PayloadAction<IQuestion[]>) => {
      state.status = "finished";
      const items = action.payload;
      state.data = {
        questionList: items,
        addQuestionModalSwitch: state.data?.addQuestionModalSwitch ?? false,
        navbarTitle: state.data?.navbarTitle ?? "لیست سوالات",
      };
    },
    setNavbarTitle: (state, action: PayloadAction<string>) => {
      state.status = "finished";
      state.data = {
        questionList: state.data?.questionList ?? [],
        addQuestionModalSwitch: state.data?.addQuestionModalSwitch ?? false,
        navbarTitle: action.payload,
      };
    },
  },
});

export const selectAllQuestions = (state: {
  questionSlice: { data: IQuestionSlice };
}) => {
  return state.questionSlice.data.questionList;
};
export const selectAddQuestionSwitch = (state: {
  questionSlice: { data: IQuestionSlice };
}) => {
  return state.questionSlice.data.addQuestionModalSwitch;
};
export const selectNavbarTitle = (state: {
  questionSlice: { data: IQuestionSlice };
}) => {
  return state.questionSlice.data.navbarTitle;
};

export const refetchQuestions = () => async (dispatch: Dispatch<any>) => {
  await axios.get("http://localhost:5000/questions").then((res) => {
    if (res.status === 200) {
      dispatch(setQuestionList(res.data));
    }
  });
};

export const { setQuestionList, addQuestionSwitch, setNavbarTitle } =
  slice.actions;

export default slice.reducer;
