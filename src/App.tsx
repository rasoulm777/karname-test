import { Provider } from "react-redux";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import MainLayout from "./layouts/MainLayout";
import QuestionList from "./pages/question-list/QuestionList";
import AnswerList from "./pages/answer-list/AnswerList";
import store from './store/store'
function App() {
  return (
    <>
      <Provider store={store}>
        <MainLayout >
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<QuestionList />} />
              <Route path="/:id" element={<AnswerList />} />
            </Routes>
          </BrowserRouter>
        </MainLayout>
      </Provider>
    </>
  );
}

export default App;
