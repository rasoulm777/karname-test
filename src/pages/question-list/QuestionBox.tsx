import { FC } from 'react'
import { useNavigate } from 'react-router';
import profile from '../../assets/images/profile.png'
import { timeFormatter } from '../../common/commonFunctions';
import { IQuestion } from '../../models/questions/questions';
import { dateFormatter } from '../../common/commonFunctions';
import Icons from '../../assets/icons/Icons';

interface IProps {
    question: IQuestion
    hasDetailBtn?: boolean
}
const QuestionBox: FC<IProps> = ({ question, hasDetailBtn = true }) => {
    const navigate = useNavigate()
    return (
        <div className='shadow-lg py-2' key={question.id}>
            <div className='grid grid-cols-2 gap-4  border py-4 px-4 bg-white'>
                <div className='flex items-center'>
                    <img src={profile} alt='profile' className='w-10 h-10 ' />
                    <h3 className='font-bold text-xl pr-2' >{question.title}</h3>
                </div>
                <div className='grid place-items-end'>
                    <div className='flex items-center'>
                        <p className='px-2' ><span className='text-gray-400'>ساعت : </span>{timeFormatter(question.date)}</p>
                        <p className='px-2 border-r-2'><span className='text-gray-400'>تاریخ : </span>{dateFormatter(question.date)}</p>
                        <button className="text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                            <Icons icon='comment'/>
                            {question.answers.length}
                        </button>
                    </div>
                </div>
            </div>
            <div className='bg-slate-100 py-2 px-2'>
                <p>{question.description}</p>
                {
                    hasDetailBtn && (
                        <div className='grid place-items-end  px-2 py-2'>
                            <button onClick={() => navigate({ pathname: `/${question.id}` })} className="bg-transparent hover:bg-green-500 text-green-600  hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded">
                                مشاهده جزییات
                            </button>
                        </div>
                    )
                }
            </div>

        </div>
    )
}

export default QuestionBox;