import { useEffect } from 'react'
import QuestionBox from './QuestionBox'
import { useDispatch, useSelector } from 'react-redux';
import { selectAllQuestions, refetchQuestions, setNavbarTitle } from '../../store/slices/questionSlice'

const QuestionList = () => {

    const dispatch = useDispatch()
    const questionList = useSelector(selectAllQuestions)


    const getQuestions = (function () {
        let executed = false;
        return function () {
            if (!executed) {
                executed = true;
                dispatch(refetchQuestions() as any)
            }
        };
    })();

    useEffect(() => {
        getQuestions()
        dispatch(setNavbarTitle("لیست سوالات"))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div>
            {questionList.map(q => <QuestionBox question={q} key={q.id} />)}
        </div>
    )
}

export default QuestionList