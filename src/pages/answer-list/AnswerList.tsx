import { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import AnswerBox from './AnswerBox'
import { IQuestion } from '../../models/questions/questions';
import QuestionBox from '../question-list/QuestionBox'
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { setNavbarTitle } from '../../store/slices/questionSlice';

const QuestionAnswer = () => {
    const params = useParams()
    const dispatch = useDispatch()
    const [question, setQuestion] = useState<IQuestion>()
    const [answer, setAnswer] = useState<string>("")
    const [answerError, setAnswerError] = useState(false)

    const getQuestion = () => {
        axios.get(`http://localhost:5000/questions/${params.id}`).then(res => setQuestion(res.data))
    }
    useEffect(() => {
        if (params.id) {
            getQuestion()
        }
        dispatch(setNavbarTitle("جزییات سوال"))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    const submit = (event: any) => {
        event.preventDefault()
        if (!!!answer) {
            setAnswerError(true)
        } else if (params.id && question) {
            const data: IQuestion = {
                ...question, answers: [...question.answers, {
                    id: String(Date.now()),
                    answer: answer,
                    date: new Date(),
                    dislikeCount: 0,
                    likeCount: 0,
                    userProfile: "../../assets/images/profile.png",
                    name: "admin"
                }]
            }
            axios.put(`http://localhost:5000/questions/${params.id}`, data).then(res => {
                if (res.status === 200) {
                    getQuestion()
                    setAnswer("")
                }
            })
        }

    }

    return (
        <div>
            {question && <QuestionBox question={question} hasDetailBtn={false} />}
            <h1 className='text-2xl font-bold my-5'>پاسخ ها</h1>
            {
                question?.answers?.map(a => (<AnswerBox answer={a} question={question} refetch={getQuestion} />))
            }
            <h1 className='text-2xl font-bold my-5'>پاسخ خود را ثبت کنید</h1>
            <form onSubmit={submit} className="relative flex-auto" >
                <div className="mb-4">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                        پاسخ خود را بنویسید
                    </label>
                    <textarea value={answer} onChange={event => { setAnswer(event.target.value); setAnswerError(false) }} placeholder='متن پاسخ ...' rows={5} className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="answer" />
                    {answerError && <p className='text-red-500 my-2'>لطفا متن پاسخ خود را بنویسید</p>}
                </div>
                <button
                    className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="submit"
                >
                    ارسال پاسخ
                </button>
            </form>
        </div>
    )
}

export default QuestionAnswer