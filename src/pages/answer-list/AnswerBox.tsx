import { FC } from 'react'
import profile from '../../assets/images/profile.png'
import { IAnswer, IQuestion } from '../../models/questions/questions';
import axios from 'axios';
import { timeFormatter } from '../../common/commonFunctions';
import { dateFormatter } from './../../common/commonFunctions';
import Icons from './../../assets/icons/Icons';

interface IProps {
    answer: IAnswer
    question: IQuestion
    refetch: () => void
}
const AnswerBox: FC<IProps> = ({ answer, question, refetch }) => {

    const likeHandler = () => {

        let data = { ...question }
        let comment = data.answers.find(x => x.id === answer.id)
        if (comment) comment.likeCount += 1
        axios.put(`http://localhost:5000/questions/${question.id}`, data).then(res => {
            if (res.status === 200) {
                refetch()
            }
        })
    }

    const disLikeHandler = () => {

        let data = { ...question }
        let comment = data.answers.find(x => x.id === answer.id)
        if (comment) comment.dislikeCount += 1
        axios.put(`http://localhost:5000/questions/${question.id}`, data).then(res => {
            if (res.status === 200) {
                refetch()
            }
        })
    }

    return (
        <div className='shadow-lg py-2' key={answer.id}>
            <div className='grid grid-cols-2 gap-4  border py-4 px-4 bg-white'>
                <div className='flex items-center'>
                    <img src={profile} alt='profile' className='w-10 h-10 ' />
                    <h3 className='font-bold text-xl pr-2' >{answer.name}</h3>
                </div>
                <div className='grid place-items-end'>
                    <div className='flex items-center'>
                        <p className='px-2' ><span className='text-gray-400'>ساعت : </span>{timeFormatter(answer.date)}</p>
                        <p className='px-2 border-r-2'><span className='text-gray-400'>تاریخ : </span>{dateFormatter(answer.date)}</p>
                        <button onClick={likeHandler} className="text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">

                            <Icons icon='like' />
                            {answer.likeCount}
                        </button>
                        <button onClick={disLikeHandler} className="text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                            <Icons icon='disLike' />
                            {answer.dislikeCount}
                        </button>
                    </div>
                </div>
            </div>
            <div className='bg-slate-100 py-2 px-2'>
                <p>{answer.answer}</p>
                <div className='flex justify-end  px-2 py-2'>
                    <button onClick={likeHandler} className="flex mx-2 items-center bg-transparent hover:bg-green-500 text-green-600  hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded">
                        <Icons icon='like' />
                        <span className='px-1'>پاسخ خوب بود</span>
                    </button>
                    <button onClick={disLikeHandler} className="flex mx-2 items-center bg-transparent hover:bg-red-500 text-red-600  hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded">
                        <Icons icon='disLike' />
                        <span className='px-1'>پاسخ خوب نبود</span>
                    </button>
                </div>
            </div>

        </div>
    )
}

export default AnswerBox;