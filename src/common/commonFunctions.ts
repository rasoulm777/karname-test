import moment from "jalali-moment";

export const dateFormatter = (date: Date) => {
    if (date) {
        return moment(date)?.format("jDD/jMM/jYYYY");
    }
};
export const timeFormatter = (date: Date) => {
    if (date) {
        return moment(date)?.format("HH:mm");
    }
};